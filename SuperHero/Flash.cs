﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace SuperHero
{
    internal class Flash : Human, IFlash
    {
        private string _name;
        public int Voltage { get; set; }
        public Flash(string name, int age, int voltage) : base(name, age)
        {
            Voltage = voltage;
        }

        public override string Name { get => _name; set => _name = value; }
        public void ActivateSuperPowers()
        {
            FireLightning();
        }

        public void FireLightning()
        {
            Console.WriteLine($"Firing Lightining at {Voltage} volt!");
        }

        public override string ToString()
        {
            return $"{base.ToString()}, voltage is: {Voltage}";
            
        }
    }
}
