﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace SuperHero
{
    class SpiderMan : Human, IClimb
    {
        private string _name;
        public int Speed { get; private set; }
        public SpiderMan(string name,int age, int speed) : base(name, age)
        {
            Speed = speed;
        }
        public override string Name { get => _name; set => _name = value; }

        public void ActivateSuperPowers()
        {
            Climb();
        }

        public void Climb()
        {
            Console.WriteLine($"Climbing at {Speed} mph");
        }

        public override string ToString()
        {
            return $"{base.ToString()}, speed is: {Speed}";
        }
    }
}
