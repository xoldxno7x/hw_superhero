﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperHero
{
    abstract class Human
    {
        public abstract string Name { get; set; }
        private int _age;
        public Human(string name, int age)
        {
            Name = name;
            _age = age;
        }

        public override string ToString()
        {
            return $"Human: [name is: {Name}, age is:{_age}]";
        }
    }
}
