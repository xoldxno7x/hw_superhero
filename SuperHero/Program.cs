﻿using System;

namespace SuperHero
{
    class Program
    {
        static void Main(string[] args)
        {
            SuperMan sm = new SuperMan("Clark Kent", 48, 123);
            SpiderMan spm = new SpiderMan("Peter Parker",56,100);
            Flash f = new Flash("Henry Allen",62,500);

         
            
            ISuperHero[] superHeroes = new ISuperHero[3] { sm, spm, f };
            foreach (ISuperHero h in superHeroes)
            {
                IdentifyHero(h);
                h.ActivateSuperPowers();
                GetMoreHeroData(h);
            }

            Console.WriteLine(CreateSuperHero("SuperMan"));
        }   

        public static ISuperHero CreateSuperHero(String s)
        {
            switch (s)
            {
                case "SpiderMan":
                    return new SpiderMan("Peter Parker", 56, 100);
                case "SuperMan":
                    return new SuperMan("Clark Kent", 48, 123);
                case "Flash":
                    return new Flash("Henry Allen", 62, 500);
                default:
                    return null;
            }
        }

        public static void IdentifyHero(ISuperHero h)
        {
            if(h is SpiderMan)
            {
                Console.WriteLine("Its Spiderman!");
            }else if (h is SuperMan)
            {
                Console.WriteLine("Its Superman!");
            }else if(h is Flash)
            {
                Console.WriteLine("Its the Flash!");
            }else if(h == null)
            {
                throw new Exception("Cannot be null!");
            }
        }

        public static void GetMoreHeroData(ISuperHero h)
        {
            if(h == null)
            {
                throw new Exception("Cannot be null!");
            }

            if (h is SpiderMan)
            {
                SpiderMan sm = h as SpiderMan;
                Console.WriteLine(sm);
            }
            else if (h is SuperMan)
            {
                SuperMan sm = h as SuperMan;
                Console.WriteLine(sm);

            }
            else if (h is Flash)
            {
                Flash sm = h as Flash;
                Console.WriteLine(sm);
            }
        }
    }
}
