﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperHero
{
    interface IFlash:ISuperHero
    {
        void FireLightning();
    }
}
