﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace SuperHero
{

    class SuperMan : Human, IFly
    {
        private string _name;
        public int WebLeft { get; set; }
        public override string Name { get => _name; set => _name = value; }

        public SuperMan(string name,int age,int webLeft) : base(name, age)
        {
            WebLeft = webLeft;
        }
        
        public void ActivateSuperPowers()
        {
            Fly();
        }

        public void Fly()
        {
            Console.WriteLine($"Flying and throwing a web left at {WebLeft} degree's... For some reason");
        }

        public override string ToString()
        {
            return $"{base.ToString()}, web left at: {WebLeft} degree's";
        }
    }
}
