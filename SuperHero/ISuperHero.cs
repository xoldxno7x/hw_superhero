﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SuperHero
{
    interface ISuperHero
    {
        void ActivateSuperPowers();
    }
}
